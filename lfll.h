#include <stddef.h>

struct lfll_head {
	struct node * _Atomic entry;
};

struct lfll_head *init_lfll(void);

void *lfll_search(struct lfll_head *head, size_t key);

void *lfll_insert(struct lfll_head *head, size_t key, void *value);

void *lfll_remove(struct lfll_head *head, size_t key);
