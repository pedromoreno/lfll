CC=gcc
CFLAGS=-Wall -fPIC
AR=ar
OPT=-O3
LFLAGS=-shared
DEBUG=-Og
SYM=-g -ggdb

.PHONY: default debug all clean

default: liblfll.a
debug: liblfll_debug.a liblfll_debug.so
sym: liblfll_sym.a liblfll_sym.so
all: liblfll.a liblfll.so

liblfll.so: lfll.o
	$(CC) lfll.o $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS) -o liblfll.so

liblfll.a: lfll.o
	$(AR) rcu liblfll.a lfll.o

lfll.o: lfll.c
	$(CC) -c lfll.c $(CFLAGS) $(INCLUDE) $(OPT) $(LFLAGS)

liblfll_debug.so: lfll_debug.o
	$(CC) lfll_debug.o $(CFLAGS) $(INCLUDE) $(DEBUG) $(SYM) $(LFLAGS) -o liblfll_debug.so

liblfll_debug.a: lfll_debug.o
	$(AR) rcu liblfll_debug.a lfll_debug.o

lfll_debug.o: lfll.c
	$(CC) -c lfll.c $(CFLAGS) $(INCLUDE) $(DEBUG) $(SYM) $(LFLAGS) -o lfll_debug.o

liblfll_sym.so: lfll_sym.o
	$(CC) lfll_sym.o $(CFLAGS) $(INCLUDE) $(OPT) $(SYM) $(LFLAGS) -o liblfll_sym.so

liblfll_sym.a: lfll_sym.o
	$(AR) rcu liblfll_sym.a lfll_sym.o

lfll_sym.o: lfll.c
	$(CC) -c lfll.c $(CFLAGS) $(INCLUDE) $(OPT) $(SYM) $(LFLAGS) -o lfll_sym.o

clean:
	rm -f *.o *.a *.so
