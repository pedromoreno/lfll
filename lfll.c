#include "lfll.h"
#include <stdatomic.h>
#include <stdbool.h>
#include <stdlib.h>

struct node {
	size_t key;
	void *value;
	struct node * _Atomic next;
};

unsigned get_flag(struct node *ptr)
{
	return (size_t) ptr & 1;
}

struct node *set_flag(struct node *ptr)
{
	return (struct node *) ((size_t) ptr | 1);
}

struct node *valid_ptr(struct node *ptr)
{
	return (struct node *) ((size_t) ptr & ~1ULL);
}

struct node * _Atomic *get_ip(struct lfll_head *head, struct node *last_valid)
{
	if(last_valid)
		return &(last_valid->next);
	else
		return &(head->entry);
}

struct lfll_head *init_lfll(void){
	struct lfll_head *head = malloc(sizeof(struct lfll_head));
	atomic_init(&(head->entry), NULL);
	return head;
}

bool find_node(
		struct lfll_head *head,
		size_t key,
		struct node **last_valid,
		struct node **last_valid_next,
		struct node **cur,
		struct node **next){
	*last_valid = NULL;
	*cur = atomic_load_explicit(&(head->entry), memory_order_consume);
	*last_valid_next = *cur;
	while(*cur){
		*next = atomic_load_explicit(
				&((*cur)->next),
				memory_order_consume);
		if((*cur)->key >= key)
			return (*cur)->key == key;
		if(get_flag(*next)){
			*cur = valid_ptr(*next);
		} else {
			*last_valid = *cur;
			*cur = *next;
			*last_valid_next = *next;
		}
	}
	return false;
}


void *lfll_insert(struct lfll_head *head, size_t key, void *value){
	struct node *last_valid,
	            *last_valid_next,
	            *cur,
	            *next,
	            *new,
	            *new_next;
	while(!find_node(
				head,
				key,
				&last_valid,
				&last_valid_next,
				&cur,
				&next) ||
	      get_flag(next)){
		if(get_flag(next))
			new_next = valid_ptr(next);
		else
			new_next = cur;
		new = malloc(sizeof(struct node));
		new->key = key;
		new->value = value;
		atomic_store_explicit(
				&(new->next),
				new_next,
				memory_order_release);
		if(atomic_compare_exchange_strong_explicit(
					get_ip(head, last_valid),
					&last_valid_next,
					new,
					memory_order_release,
					memory_order_relaxed))
			return value;
	}
	return cur->value;
}


void *lfll_search(struct lfll_head *head, size_t key){
	struct node *last_valid,
	            *last_valid_next,
	            *cur,
	            *next;
	if(find_node(head, key, &last_valid, &last_valid_next, &cur, &next) &&
	   !get_flag(next))
		return cur->value;
	else
		return NULL;
}


void *lfll_remove(struct lfll_head *head, size_t key){
	struct node *last_valid,
	            *last_valid_next,
	            *cur,
	            *next;
	if(!find_node(head, key, &last_valid, &last_valid_next, &cur, &next) ||
	   get_flag(next))
		return NULL;
	if(!atomic_compare_exchange_strong_explicit(
				&(cur->next),
				&next,
				set_flag(next),
				memory_order_release,
				memory_order_relaxed)){
		if(get_flag(next))
			return NULL;
		else
			return lfll_remove(head, key);
	} else {
		void *value = cur->value;
		struct node *inv_cur = cur;
		do{
			if(atomic_compare_exchange_strong_explicit(
						get_ip(head, last_valid),
						&last_valid_next,
						valid_ptr(next),
						memory_order_release,
						memory_order_relaxed))
				break;
		} while(find_node(
					head,
					key,
					&last_valid,
					&last_valid_next,
					&cur,
					&next) &&
		        inv_cur == cur &&
		        get_flag(next));
		return value;
	}
}
